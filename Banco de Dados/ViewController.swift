//
//  ViewController.swift
//  Banco de Dados
//
//  Created by IFPB on 17/12/20.
//  Copyright © 2020 Mauricio Pereira. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    @IBOutlet weak var tfNome: UITextField!
    @IBOutlet weak var tfIdade: UITextField!
    var delegate: AppDelegate!
    var contexto: NSManagedObjectContext!
    var pessoas: Array<NSManagedObject>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = UIApplication.shared.delegate as? AppDelegate
        self.contexto = self.delegate.persistentContainer.viewContext
        
        let requisicao = NSFetchRequest<NSFetchRequestResult>(entityName: "Pessoa")
        
        do {
            self.pessoas = try self.contexto.fetch(requisicao) as? [NSManagedObject]
        } catch {
            print("Erro")
        }
    }
    
    @IBAction func deletar(_ sender: Any) {
        
        // precisa implementar uma forma de visualizar os dados em uma lista e recuperar o index desse objeto antes de excluir
        let pessoa = self.pessoas[0]
        self.pessoas.remove(at: 0)
        self.contexto.delete(pessoa)
        self.delegate.saveContext()
        
        self.listar()
    }
    
    @IBAction func atualizar(_ sender: Any) {
        let nome = self.tfNome.text
        let idade = Int(self.tfIdade.text!)
        
        // precisa implementar uma forma de visualizar os dados em uma lista e recuperar o index desse objeto antes de atualizar
        let pessoa = self.pessoas[0]
        
        pessoa.setValue(nome, forKey: "nome")
        pessoa.setValue(idade, forKey: "idade")
        
        self.delegate.saveContext()
        
        //implementar o reload da lista
        self.listar()
    }
    
    @IBAction func salvar(_ sender: Any) {
        let nome = self.tfNome.text
        let idade = Int(self.tfIdade.text!)
        let pessoa = NSEntityDescription.insertNewObject(forEntityName: "Pessoa", into: self.contexto)
        
        pessoa.setValue(nome, forKey: "nome")
        pessoa.setValue(idade, forKey: "idade")
        
        self.delegate.saveContext()
        
        self.listar()
    }
    
    func listar() {
        let requisicao = NSFetchRequest<NSFetchRequestResult>(entityName: "Pessoa")
        
        do {
            self.pessoas = try self.contexto.fetch(requisicao) as? [NSManagedObject]
            for p in self.pessoas {
                let nome = p.value(forKey: "nome")
                let idade = p.value(forKey: "idade")
                print("\(nome!) - \(idade!)")
            }
        } catch {
            print("Erro")
        }
    }
}

